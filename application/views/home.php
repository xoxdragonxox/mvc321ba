<?php



?>

<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <title>ToDo-Home</title>
    </head>
    <body>
        <div id='header'></div>
        
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <h1 id='title'>ToDo</h1>
                </div>
                <div class="col-sm-2" style="float: right;">
                    <img id='profile' src="img/defaultprofile.png" height="50px" width="50px">
                </div>
                <div id='login' class="col-sm-1" style="float: right;">
                    <a>LOGIN</a>
                </div>
                <div id='resister' class="col-sm-1" style="float: right;">
                    <a>REGISTER</a>
                </div>
            </div>
        </div>
    </body>
</html>